/* eslint-disable */
/* tslint:disable */
/* auto-generated vue proxies */
import { defineContainer } from './vue-component-lib/utils';

import type { JSX } from '@kensodemann/game-dots/components';

import { defineCustomElement as defineKwsCircleMarker } from '@kensodemann/game-dots/components/kws-circle-marker.js';
import { defineCustomElement as defineKwsMatchGrid } from '@kensodemann/game-dots/components/kws-match-grid.js';


export const KwsCircleMarker = /*@__PURE__*/ defineContainer<JSX.KwsCircleMarker>('kws-circle-marker', defineKwsCircleMarker, [
  'size',
  'boxColor',
  'color'
]);


export const KwsMatchGrid = /*@__PURE__*/ defineContainer<JSX.KwsMatchGrid>('kws-match-grid', defineKwsMatchGrid, [
  'size',
  'markers',
  'boxColor',
  'exactMatchColor',
  'misplacedMatchColor',
  'exactMatches',
  'misplacedMatches'
]);

