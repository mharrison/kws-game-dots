import { newE2EPage } from '@stencil/core/testing';

describe('kws-circle-marker', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<kws-circle-marker></kws-circle-marker>');

    const element = await page.find('kws-circle-marker');
    expect(element).toHaveClass('hydrated');
  });
});
