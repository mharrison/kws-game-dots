import { newSpecPage } from '@stencil/core/testing';
import { KwsCircleMarker } from '../kws-circle-marker';

describe('kws-circle-marker', () => {
  it('renders defaults', async () => {
    const page = await newSpecPage({
      components: [KwsCircleMarker],
      html: `<kws-circle-marker></kws-circle-marker>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-circle-marker>
        <mock:shadow-root>
          <svg width="32" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="28" height="28" rx="8" ry="8" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="16" cy="16" r="8" fill="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-circle-marker>
    `);
  });

  it.each([
    [24, 20, 12, 5],
    [32, 28, 16, 8],
    [48, 44, 24, 13],
    [64, 60, 32, 19],
    [128, 124, 64, 40],
  ])('renders for size %s', async (size: number, length: number, center: number, radius: number) => {
    const page = await newSpecPage({
      components: [KwsCircleMarker],
      html: `<kws-circle-marker size="${size}"></kws-circle-marker>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-circle-marker size="${size}">
        <mock:shadow-root>
          <svg width="${size}" height="${size}" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="${length}" height="${length}" rx="8" ry="8" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="${center}" cy="${center}" r="${radius}" fill="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-circle-marker>
    `);
  });

  it('renders with the proper box color', async () => {
    const page = await newSpecPage({
      components: [KwsCircleMarker],
      html: `<kws-circle-marker box-color="red"></kws-circle-marker>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-circle-marker box-color="red">
        <mock:shadow-root>
          <svg width="32" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="28" height="28" rx="8" ry="8" stroke-width="2" stroke="red" fill="transparent"></rect>
            <circle cx="16" cy="16" r="8" fill="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-circle-marker>
    `);
  });

  it('renders with the proper color', async () => {
    const page = await newSpecPage({
      components: [KwsCircleMarker],
      html: `<kws-circle-marker color="purple"></kws-circle-marker>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-circle-marker color="purple">
        <mock:shadow-root>
          <svg width="32" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="28" height="28" rx="8" ry="8" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="16" cy="16" r="8" fill="purple"></circle>
          </svg>
        </mock:shadow-root>
      </kws-circle-marker>
    `);
  });
});
