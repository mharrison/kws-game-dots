# kws-circle-marker

<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description                                                                                                                      | Type     | Default  |
| ---------- | ----------- | -------------------------------------------------------------------------------------------------------------------------------- | -------- | -------- |
| `boxColor` | `box-color` | The box color determines the color of the outside rectangle. It can be specified in any valid CSS format like 'red' or '#0032ab' | `string` | `'#000'` |
| `color`    | `color`     | The color determines the color of the marble. It can be specified in any valid CSS format like 'red' or '#0032ab'                | `string` | `'#000'` |
| `size`     | `size`      | The size determines how big the square is.                                                                                       | `number` | `32`     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
