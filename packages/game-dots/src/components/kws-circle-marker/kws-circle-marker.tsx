import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'kws-circle-marker',
  styleUrl: 'kws-circle-marker.css',
  shadow: true,
})
export class KwsCircleMarker {
  /**
   * The size determines how big the square is.
   *
   * @default 32
   */
  @Prop() size: number = 32;

  /**
   * The box color determines the color of the outside rectangle.
   * It can be specified in any valid CSS format like 'red' or '#0032ab'
   *
   * @default #000
   */
  @Prop() boxColor: string = '#000';

  /**
   * The color determines the color of the marble.
   * It can be specified in any valid CSS format like 'red' or '#0032ab'
   *
   * @default #000
   */
  @Prop() color: string = '#000';

  render() {
    return (
      <svg width={this.size} height={this.size} version="1.1" xmlns="http://www.w3.org/2000/svg">
        <rect
          x="2"
          y="2"
          width={this.size - 4}
          height={this.size - 4}
          rx="8"
          ry="8"
          stroke-width="2"
          stroke={this.boxColor}
          fill="transparent"
        ></rect>
        <circle cx={this.size / 2} cy={this.size / 2} r={Math.round((this.size - 8) / 3)} fill={this.color}></circle>
      </svg>
    );
  }
}
