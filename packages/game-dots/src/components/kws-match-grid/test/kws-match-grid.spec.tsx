import { newSpecPage } from '@stencil/core/testing';
import { KwsMatchGrid } from '../kws-match-grid';

describe('kws-match-grid', () => {
  it('renders with default properties', async () => {
    const page = await newSpecPage({
      components: [KwsMatchGrid],
      html: `<kws-match-grid></kws-match-grid>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-match-grid>
        <mock:shadow-root>
          <svg width="32" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="28" height="28" rx="4" ry="4" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="9" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="9" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="23" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="23" cy="23" r="4" fill="transparent" stroke="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-match-grid>
    `);
  });

  it('renders with the box color', async () => {
    const page = await newSpecPage({
      components: [KwsMatchGrid],
      html: `<kws-match-grid box-color="red"></kws-match-grid>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-match-grid box-color="red">
        <mock:shadow-root>
          <svg width="32" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="28" height="28" rx="4" ry="4" stroke-width="2" stroke="red" fill="transparent"></rect>
            <circle cx="9" cy="9" r="4" fill="transparent" stroke="red"></circle>
            <circle cx="9" cy="23" r="4" fill="transparent" stroke="red"></circle>
            <circle cx="23" cy="9" r="4" fill="transparent" stroke="red"></circle>
            <circle cx="23" cy="23" r="4" fill="transparent" stroke="red"></circle>
          </svg>
        </mock:shadow-root>
      </kws-match-grid>
    `);
  });

  it('renders one marker', async () => {
    const page = await newSpecPage({
      components: [KwsMatchGrid],
      html: `<kws-match-grid markers="1"></kws-match-grid>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-match-grid markers="1">
        <mock:shadow-root>
          <svg width="32" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="28" height="28" rx="4" ry="4" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="9" cy="9" r="4" fill="transparent" stroke="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-match-grid>
    `);
  });

  it('renders three markers', async () => {
    const page = await newSpecPage({
      components: [KwsMatchGrid],
      html: `<kws-match-grid markers="3"></kws-match-grid>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-match-grid markers="3">
        <mock:shadow-root>
          <svg width="32" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="28" height="28" rx="4" ry="4" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="9" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="9" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="23" cy="9" r="4" fill="transparent" stroke="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-match-grid>
    `);
  });

  it('renders six markers', async () => {
    const page = await newSpecPage({
      components: [KwsMatchGrid],
      html: `<kws-match-grid markers="6"></kws-match-grid>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-match-grid markers="6">
        <mock:shadow-root>
          <svg width="46" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="42" height="28" rx="4" ry="4" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="9" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="9" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="23" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="23" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="37" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="37" cy="23" r="4" fill="transparent" stroke="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-match-grid>
    `);
  });

  it('renders seven markers', async () => {
    const page = await newSpecPage({
      components: [KwsMatchGrid],
      html: `<kws-match-grid markers="7"></kws-match-grid>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-match-grid markers="7">
        <mock:shadow-root>
          <svg width="60" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="56" height="28" rx="4" ry="4" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="9" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="9" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="23" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="23" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="37" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="37" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="51" cy="9" r="4" fill="transparent" stroke="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-match-grid>
    `);
  });

  it('renders eight markers', async () => {
    const page = await newSpecPage({
      components: [KwsMatchGrid],
      html: `<kws-match-grid markers="8"></kws-match-grid>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-match-grid markers="8">
        <mock:shadow-root>
          <svg width="60" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="56" height="28" rx="4" ry="4" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="9" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="9" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="23" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="23" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="37" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="37" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="51" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="51" cy="23" r="4" fill="transparent" stroke="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-match-grid>
    `);
  });

  it('renders misplaced and exact matches', async () => {
    const page = await newSpecPage({
      components: [KwsMatchGrid],
      html: `<kws-match-grid markers="8" exact-matches="3" misplaced-matches="2" ></kws-match-grid>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-match-grid markers="8" exact-matches="3" misplaced-matches="2">
        <mock:shadow-root>
          <svg width="60" height="32" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="56" height="28" rx="4" ry="4" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="9" cy="9" r="4" fill="#3cb371" stroke="#3cb371"></circle>
            <circle cx="9" cy="23" r="4" fill="#3cb371" stroke="#3cb371"></circle>
            <circle cx="23" cy="9" r="4" fill="#3cb371" stroke="#3cb371"></circle>
            <circle cx="23" cy="23" r="4" fill="#777" stroke="#777"></circle>
            <circle cx="37" cy="9" r="4" fill="#777" stroke="#777"></circle>
            <circle cx="37" cy="23" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="51" cy="9" r="4" fill="transparent" stroke="#000"></circle>
            <circle cx="51" cy="23" r="4" fill="transparent" stroke="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-match-grid>
    `);
  });

  it.each([
    [48, 44, 13, 35, 6],
    [64, 60, 17, 47, 8],
    [128, 124, 33, 95, 16],
  ])('scales rendering for size %s', async (size: number, length: number, p1: number, p2: number, radius: number) => {
    const page = await newSpecPage({
      components: [KwsMatchGrid],
      html: `<kws-match-grid size=${size}></kws-match-grid>`,
    });
    expect(page.root).toEqualHtml(`
      <kws-match-grid size="${size}">
        <mock:shadow-root>
          <svg width="${size}" height="${size}" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect x="2" y="2" width="${size - 4}" height="${
      size - 4
    }" rx="4" ry="4" stroke-width="2" stroke="#000" fill="transparent"></rect>
            <circle cx="${p1}" cy="${p1}" r="${radius}" fill="transparent" stroke="#000"></circle>
            <circle cx="${p1}" cy="${p2}" r="${radius}" fill="transparent" stroke="#000"></circle>
            <circle cx="${p2}" cy="${p1}" r="${radius}" fill="transparent" stroke="#000"></circle>
            <circle cx="${p2}" cy="${p2}" r="${radius}" fill="transparent" stroke="#000"></circle>
          </svg>
        </mock:shadow-root>
      </kws-match-grid>
    `);
  });
});
