import { newE2EPage } from '@stencil/core/testing';

describe('kws-match-grid', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<kws-match-grid></kws-match-grid>');

    const element = await page.find('kws-match-grid');
    expect(element).toHaveClass('hydrated');
  });
});
