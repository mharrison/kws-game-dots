import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'kws-match-grid',
  styleUrl: 'kws-match-grid.css',
  shadow: true,
})
export class KwsMatchGrid {
  /**
   * The size determines how big the square is.
   *
   * @default 32
   */
  @Prop() size: number = 32;

  /**
   * The markers determines the number of scoring markers to draw.
   *
   * @default 4
   */
  @Prop() markers: number = 4;

  /**
   * The box color determines the color of the outside rectangle as well as the default (non-match) circles.
   * It can be specified in any valid CSS format like 'red' or '#0032ab'
   *
   * @default #000
   */
  @Prop() boxColor: string = '#000';

  /**
   * The exact match color determines the color of fill to use to indicate an item that is in the solution in the proper location.
   * It can be specified in any valid CSS format like 'red' or '#0032ab'
   *
   * @default #000
   */
  @Prop() exactMatchColor: string = '#3cb371';

  /**
   * The misplaced match color determines the color of fill to use to indicate an item that is in the solution but in the wrong location.
   * It can be specified in any valid CSS format like 'red' or '#0032ab'
   *
   * @default #000
   */
  @Prop() misplacedMatchColor: string = '#777';

  /**
   * The exact matches specifies the number of matching items in the proper location.
   *
   * @default 0
   */
  @Prop() exactMatches: number = 0;

  /**
   * The misplaced matches specifies the number of matching items in the wrong location.
   *
   * @default 0
   */
  @Prop() misplacedMatches: number = 0;

  render() {
    const radius = this.size / 8;
    const start = (this.size - 4) / 4 + 2;
    const delta = (this.size - 4) / 2;
    const markerElements = [];
    const extraColumns = this.markers < 5 ? 0 : Math.ceil((this.markers - 4) / 2);
    const totalMatches = this.exactMatches + this.misplacedMatches;

    for (let i = 0; i < this.markers; i++) {
      const col = Math.floor(i / 2);
      const topRow = i % 2 === 0;
      markerElements.push(
        <circle
          cx={start + col * delta}
          cy={start + (topRow ? 0 : delta)}
          r={radius}
          fill={
            i < this.exactMatches ? this.exactMatchColor : i < totalMatches ? this.misplacedMatchColor : 'transparent'
          }
          stroke={
            i < this.exactMatches ? this.exactMatchColor : i < totalMatches ? this.misplacedMatchColor : this.boxColor
          }
        ></circle>
      );
    }

    return (
      <svg width={this.size + extraColumns * delta} height={this.size} version="1.1" xmlns="http://www.w3.org/2000/svg">
        <rect
          x="2"
          y="2"
          width={this.size + extraColumns * delta - 4}
          height={this.size - 4}
          rx="4"
          ry="4"
          stroke-width="2"
          stroke={this.boxColor}
          fill="transparent"
        ></rect>
        {markerElements}
      </svg>
    );
  }
}
