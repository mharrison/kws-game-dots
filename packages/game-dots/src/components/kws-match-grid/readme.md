# kws-match-grid

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute               | Description                                                                                                                                                                                              | Type     | Default     |
| --------------------- | ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | ----------- |
| `boxColor`            | `box-color`             | The box color determines the color of the outside rectangle as well as the default (non-match) circles. It can be specified in any valid CSS format like 'red' or '#0032ab'                              | `string` | `'#000'`    |
| `exactMatchColor`     | `exact-match-color`     | The exact match color determines the color of fill to use to indicate an item that is in the solution in the proper location. It can be specified in any valid CSS format like 'red' or '#0032ab'        | `string` | `'#3cb371'` |
| `exactMatches`        | `exact-matches`         | The exact matches specifies the number of matching items in the proper location.                                                                                                                         | `number` | `0`         |
| `markers`             | `markers`               | The markers determines the number of scoring markers to draw.                                                                                                                                            | `number` | `4`         |
| `misplacedMatchColor` | `misplaced-match-color` | The misplaced match color determines the color of fill to use to indicate an item that is in the solution but in the wrong location. It can be specified in any valid CSS format like 'red' or '#0032ab' | `string` | `'#777'`    |
| `misplacedMatches`    | `misplaced-matches`     | The misplaced matches specifies the number of matching items in the wrong location.                                                                                                                      | `number` | `0`         |
| `size`                | `size`                  | The size determines how big the square is.                                                                                                                                                               | `number` | `32`        |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
