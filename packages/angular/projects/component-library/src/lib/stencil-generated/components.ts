/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';
import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import type { Components } from '@kensodemann/game-dots/components';

import { defineCustomElement as defineKwsCircleMarker } from '@kensodemann/game-dots/components/kws-circle-marker.js';
import { defineCustomElement as defineKwsMatchGrid } from '@kensodemann/game-dots/components/kws-match-grid.js';


export declare interface KwsCircleMarker extends Components.KwsCircleMarker {}

@ProxyCmp({
  defineCustomElementFn: defineKwsCircleMarker,
  inputs: ['boxColor', 'color', 'size']
})
@Component({
  selector: 'kws-circle-marker',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['boxColor', 'color', 'size']
})
export class KwsCircleMarker {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface KwsMatchGrid extends Components.KwsMatchGrid {}

@ProxyCmp({
  defineCustomElementFn: defineKwsMatchGrid,
  inputs: ['boxColor', 'exactMatchColor', 'exactMatches', 'markers', 'misplacedMatchColor', 'misplacedMatches', 'size']
})
@Component({
  selector: 'kws-match-grid',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['boxColor', 'exactMatchColor', 'exactMatches', 'markers', 'misplacedMatchColor', 'misplacedMatches', 'size']
})
export class KwsMatchGrid {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}
