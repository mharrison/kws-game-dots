
import * as d from './components';

export const DIRECTIVES = [
  d.KwsCircleMarker,
  d.KwsMatchGrid
];
