# Framework Wrapper: Angular

Web components are well supported by Angular via the [CUSTOM_ELEMENTS_SCHEMA](https://angular.io/api/core/CUSTOM_ELEMENTS_SCHEMA). Use of this schema, however, is not ideal as it can mask various compile type errors when parsing the templates. It would also require the consumer of our library to import and call the function(s) that define our web components within the DOM. To avoid this, we will create a set of proxies that wrap our components and an `NgModule` that allows for easy consumption of them.

To do this, we use the [Stencil Angular Output Target](https://www.npmjs.com/package/@stencil/angular-output-target). The configuration we follow here is a specific implementation of the [more general documentation](https://stenciljs.com/docs/angular) found on the Stencil site.

## Create the Project

The first thing we need to do is generate the project.

```bash
cd packages
npx -p @angular/cli ng new angular --no-create-application
cd angular
npx -p @angular/cli ng generate library component-library
```

## Adjust the Configuration

The generated configuration applies to a generic Angular project. We need top modify the configuration to allow us to publish a single library.

We need to:

- Change the build output location to `dist/`
- Update the `package.json` file for publication 

### Change the Build Output Location

Open the `projects/component-library/ng-package.json` file and find the `dest` value. Change it to `../../dist`. Everything else in that file can stay the same.

### Update the `package.json` File

The existing `package.json` file is not appropriate for publishing a library to a registry. Make the following modifications.

1. Move all of the existing `dependencies` to be included in the `devDependencies`.
1. Update the `name` to match the scope and name that these wrappers will be published under (for example: `@kensodemann/game-dots-angular`).
1. Add the `description`, `license`, `homepage`, `repository`, `module`, `types`, `files`, `peerDependencies`, and `publishConfig` values (see bellow for a sample).
1. Add our Stencil project as the sole dependency. Run a command such as the following (adjust for your own package names):
   `npx lerna add @kensodemann/game-dots --scope=@kensodemann/game-dots-angular`

When you are finished, your `package.json` file should look something like this:

```JSON
{
  "name": "@kensodemann/game-dots-angular",
  "description": "Angular specific proxies for @kensodemann/game-dots",
  "license": "MIT",
  "version": "0.0.0",
  "homepage": "https://bitbucket.org/kensodemann/kws-game-dots#readme",
  "repository": {
    "type": "git",
    "url": "git+ssh://git@bitbucket.org/kensodemann/kws-game-dots.git"
  },
  "scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build",
    "watch": "ng build --watch --configuration development",
    "test": "ng test"
  },
  "module": "dist/",
  "types": "dist/component-library.d.ts",
  "files": [
    "dist/"
  ],
  "dependencies": {
    "@kensodemann/game-dots": "^0.0.0"
  },
  "devDependencies": {
    ...
  },
  "peerDependencies": {
    "@angular/core": ">=13.3.0",
    "@angular/forms": ">=13.3.0",
    "@angular/router": ">=13.3.0",
    "rxjs": ">=7.5.0",
    "zone.js": ">=0.11.4"
  },
  "publishConfig": {
    "access": "public"
  }
}
```

## Install the Framework Wrapper

Switch back to the Stencil project and add the Angular Framework Wrapper. As always, adjust the following commands for your own directory structure.

```bash
cd ../game-dots
npm i -D @stencil/angular-output-target
```

The Angular Framework Wrapper is configured in a similar manner to the other output targets. We will use a set of options that allow us to export the output of the `dist-custom-elements` build. That is, we are publishing the ES6 modules. See the [framework wrapper documentation](https://github.com/ionic-team/stencil-ds-output-targets/blob/main/packages/angular-output-target/README.md) for a full set of options.

```typescript
import { Config } from '@stencil/core';
import { angularOutputTarget as angular } from '@stencil/angular-output-target';
...

export const config: Config = {
  ...
  outputTargets: [
    angular({
      componentCorePackage: '@kensodemann/game-dots',
      directivesProxyFile: '../angular/projects/component-library/src/lib/stencil-generated/components.ts',
      directivesArrayFile: '../angular/projects/component-library/src/lib/stencil-generated/index.ts',
      includeImportCustomElements: true,
    }),
    ...
  ]
}
```

Run a build from the `packages/game-dots` directory. It should succeed, and the files in `../angular/projects/component-library/src/lib/stencil-generated` should be successfully created. It is now time to clean up the Angular library.

## Clean up the Angular Library

Switching back to the Angular library (`cd ../angular`), we need to perform the following cleanup:

- Add our own `NgModule`.
- Export our items from the `public-api.ts` file.
- Remove the boilerplate code that was generated by the Angular CLI.

### Create Our `NgModule`

Create a `projects/component-library/src/lib/kws-game-dots.module.ts` file with the following contents:

```typescript
import { NgModule } from '@angular/core';
import { DIRECTIVES } from './stencil-generated';

@NgModule({
  declarations: [...DIRECTIVES],
  exports: [...DIRECTIVES],
})
export class KwsGameDotsModule {}
```

### Export Our Library Code

Open `projects/component-library/src/public-api.ts` in the editor. Remove the existing exports, and replace them with the following:

```typescript
export * from './lib/stencil-generated/components';
export * from './lib/kws-game-dots.module';
```

### Remove the Boilerplate Code

At this point, it should be safe to remove the unused boilerplate code.

```bash
rm projects/component-library/src/lib/component-library.*
```

## Notes

1. See the [consumer usage](https://stenciljs.com/docs/angular#consumer-usage) section of the documentation for tips on how to efficiently consume the package in an Angular application.
1. In the future, it may be easier to create Single Component Angular Modules (SCAMs) from this project.
1. You may or may not want to back down the Angular version used. For example, in creating this README, I went with the versions the NG CLI applied at the time. For my actual project, I backed it down to version `13.1.0`. This is an engineering decision based on what you need to support combined with what is realistic to actually support. Generally, the last one or two major versions of Angular will be a solid choice.
1. This document was written using the naming conventions within this project. You can (and should) adjust for your own project.

Happy Coding!! 🤓
