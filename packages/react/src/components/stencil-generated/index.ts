/* eslint-disable */
/* tslint:disable */
/* auto-generated react proxies */
import { createReactComponent } from './react-component-lib';

import type { JSX } from '@kensodemann/game-dots/components';

import { defineCustomElement as defineKwsCircleMarker } from '@kensodemann/game-dots/components/kws-circle-marker.js';
import { defineCustomElement as defineKwsMatchGrid } from '@kensodemann/game-dots/components/kws-match-grid.js';

export const KwsCircleMarker = /*@__PURE__*/createReactComponent<JSX.KwsCircleMarker, HTMLKwsCircleMarkerElement>('kws-circle-marker', undefined, undefined, defineKwsCircleMarker);
export const KwsMatchGrid = /*@__PURE__*/createReactComponent<JSX.KwsMatchGrid, HTMLKwsMatchGridElement>('kws-match-grid', undefined, undefined, defineKwsMatchGrid);
